import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Login me',
      home: Scaffold(
        body: LoginScreen(),
      ),
    );
  }

}

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }

}

class LoginState extends State<StatefulWidget> {
  final formKey = GlobalKey<FormState>();
  late String email;
  late String password;



  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: Form(
        key: formKey,
        child: Column(
          children: [
            emailField(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            passwordField(),
            Container(margin: EdgeInsets.only(top: 40.0),),
            loginButton()
          ],
        ),
      ),
    );
  }

  Widget emailField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: 'Email address'
      ),
      validator: (value) {
        if (!value!.contains('@')) {
          return "Pls input valid email.";
        }
        if (!value.contains('.')) {
          return "please input valid email.";
        }


        return null;
      },

      onSaved: (value) {
        email = value as String;
      },

    );
  }

  Widget passwordField() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
          icon: Icon(Icons.password),
          labelText: 'Password'
      ),
      validator: (value) {
        RegExp upper = RegExp(r'^(?=.*[A-Z])');
        RegExp lower = RegExp(r'^(?=.*[a-z])');
        RegExp special_character = RegExp(r'^(?=.*?[!@#\$&*~.])');
        if (value!.length < 8) {
          return "Password has at least 8 characters.";
        }

        else if (!upper.hasMatch(value))
        {
          return "at least 1 capital";
        }

        else if(!lower.hasMatch(value))
        {
          return "at least 1 lowercase";
        }

        else if(!special_character.hasMatch(value))
        {
          return "at least 1 special character";
        }

        return null;
      },
      onSaved: (value) {
        password = value as String;
      },
    );
  }

  Widget loginButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            formKey.currentState!.save();

            print('email=$email');
            print('Demo only: password=$password');
          }
        },
        child: Text('Login')
    );
  }
}